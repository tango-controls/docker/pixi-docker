FROM ghcr.io/prefix-dev/pixi:0.20.1

LABEL MAINTAINER "TANGO Controls Team <contact@tango-controls.org>"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y \
  && apt-get install -y --no-install-recommends  \
    curl \
    git \
    docker.io \
  && rm -rf /var/lib/apt/lists/*

RUN useradd -l -ms /bin/bash tango \
  && usermod -a -G docker,sudo tango \
  && echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

USER tango

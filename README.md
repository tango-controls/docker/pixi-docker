# pixi-docker docker image

[pixi] [docker] image to be used in CI. It includes `docker` for cpptango tests.

Docker pull command:

```bash
docker pull registry.gitlab.com/tango-controls/docker/pixi-docker
```

[docker]: https://www.docker.com
[pixi]: https://pixi.sh
